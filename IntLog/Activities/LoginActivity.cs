﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using IntLog.EventListeners;
using IntLog.Model;

namespace IntLog.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/IntLogBaixaTheme", MainLauncher = false ,NoHistory = false)]
    public class LoginActivity : AppCompatActivity
    {
        private TextInputLayout emailText;
        private TextInputLayout passwordText;
        private Button loginButton;
        private TextView clickToRegisterText;
        private CoordinatorLayout rootView;
        private CheckBox checkBoxDados;

        private User usuario;
        private FirebaseAuth mAuth;
        ISharedPreferences preferences = Application.Context.GetSharedPreferences("userinfo", FileCreationMode.Private);
        ISharedPreferencesEditor editor;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            
            SetContentView(Resource.Layout.login);
            UserDialogs.Init(this);
            emailText = (TextInputLayout) FindViewById(Resource.Id.emailText);
            passwordText = (TextInputLayout) FindViewById(Resource.Id.senhaText);
            rootView = (CoordinatorLayout) FindViewById(Resource.Id.rootView);
            loginButton = (Button) FindViewById(Resource.Id.loginButton);
            checkBoxDados = (CheckBox) FindViewById(Resource.Id.cbDados);
            clickToRegisterText = (TextView) FindViewById(Resource.Id.clickToRegisterText);
            clickToRegisterText.Click += ClickToRegisterText_Click;

            if (preferences.GetBoolean("dados", false))
            {
                emailText.EditText.Text = preferences.GetString("email", null);
                passwordText.EditText.Text = preferences.GetString("password", null);
                checkBoxDados.Checked = preferences.GetBoolean("dados", false);
            }

            loginButton.Click += LoginButtonOnClick;
            IniciaFirebase();
        }

        private void ClickToRegisterText_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(RegisterActivity));
        }

        private void LoginButtonOnClick(object sender, EventArgs e)
        {
            usuario = new User(null,null, null, emailText.EditText.Text, passwordText.EditText.Text);

            if (!usuario.Email.Contains("@"))
            {
                Snackbar.Make(rootView, "Entre com um email Valido!", Snackbar.LengthShort).Show();
                return;
            }
            else if (usuario.Senha.Length < 8)
            {
                Snackbar.Make(rootView, "Entre com uma senha Valida!", Snackbar.LengthShort).Show();
                return;
            }
            TaskCompletionListener taskCompletionListener = new TaskCompletionListener();
            taskCompletionListener.Sucess += TaskCompletionListener_Sucess;
            taskCompletionListener.Failure += TaskCompletionListenerOnFailure;

            UserDialogs.Instance.ShowLoading("Carregando...", MaskType.Black);
            mAuth.SignInWithEmailAndPassword(usuario.Email, usuario.Senha)
                .AddOnCompleteListener(taskCompletionListener)
                .AddOnFailureListener(taskCompletionListener)
                .AddOnSuccessListener(taskCompletionListener);
        }

        private void TaskCompletionListenerOnFailure(object sender, EventArgs e)
        {
            UserDialogs.Instance.HideLoading();
            Snackbar.Make(rootView,$"Login falhou!! Erro: {((TaskCompletionListener)sender).Message}",Snackbar.LengthShort).Show();
        }

        private void TaskCompletionListener_Sucess(object sender, EventArgs e)
        {
            if(checkBoxDados.Checked)
                SalvaDados();
            else
            {
                limpaDados();
            }
            UserDialogs.Instance.HideLoading();
            var preferences =
            Application.Context.GetSharedPreferences("userinfo", FileCreationMode.Private);
            var editor = preferences.Edit();
            editor.PutBoolean("login", false);
            editor.Commit();
            StartActivity(typeof(MainActivity));
            Finish();
        }

        void SalvaDados()
        {

                preferences =
                    Application.Context.GetSharedPreferences("userinfo", FileCreationMode.Private);
                editor = preferences.Edit();

                editor.PutString("email", usuario.Email);
                editor.PutString("password", usuario.Senha);
                editor.PutBoolean("dados", checkBoxDados.Checked);
                //editor.PutString("email", usuario.Email);
                //editor.PutString("fullname", usuario.Nome);
                //editor.PutString("phone", usuario.Telefone);
                //editor.PutString("cpf", usuario.Cpf);
                editor.Apply();
        }

        void limpaDados()
        {
            preferences =
                Application.Context.GetSharedPreferences("userinfo", FileCreationMode.Private);
            editor = preferences.Edit();

            editor.PutString("email", null);
            editor.PutString("password", null);
            editor.PutBoolean("dados", false);
            editor.PutBoolean("login", false);
            editor.Apply();
        }

        void IniciaFirebase()
        {
            FirebaseApp app = FirebaseApp.InitializeApp(this);
            if (app == null)
            {
                var option = new FirebaseOptions.Builder()
                    .SetApplicationId("uber-clone-e8f14")
                    .SetApiKey("AIzaSyC9O5HC7oVuVTwF6WrYbFu3I1KaxxWtBvo")
                    .SetDatabaseUrl(@"https://uber-clone-e8f14.firebaseio.com")
                    .SetStorageBucket("uber-clone-e8f14.appspot.com")
                    .Build();

                app = FirebaseApp.InitializeApp(this, option);
                mAuth = FirebaseAuth.Instance;
                ;
            }
            else
            {
                mAuth = FirebaseAuth.Instance;
                
            }

        }
    }
}
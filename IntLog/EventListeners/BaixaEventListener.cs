﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using IntLog.Helpers;
using IntLog.Model;

namespace IntLog.EventListeners
{
    class BaixaEventListener : Java.Lang.Object, IValueEventListener
    {
        List<BaixaDetails> baixaDetailsList = new List<BaixaDetails>();
        public event EventHandler<baixaDataEventArgs> baixaRetrived;
        public class baixaDataEventArgs : EventArgs
        {
            public List<BaixaDetails> baixa { get; set; }
        }
        public new void Dispose()
        {
            baixaDetailsList.Clear();
        }

        public void OnCancelled(DatabaseError error)
        {
            throw new NotImplementedException();
        }

        public void OnDataChange(DataSnapshot snapshot)
        {
            if(snapshot.Value != null)
            {
                var child = snapshot.Children.ToEnumerable<DataSnapshot>();
                baixaDetailsList.Clear();
                foreach(DataSnapshot baixaData in child)
                {
                    var baixa = new BaixaDetails();
                    baixa.DateTime = DateTime.ParseExact(baixaData.Child("datetime").Value.ToString(),"dd/MM/yyyy HH:mm:ss",System.Globalization.CultureInfo.InvariantCulture);
                    baixa.IRL = baixaData.Child("irl").Value.ToString();
                    baixa.PickupLat = Convert.ToDouble(baixaData.Child("latitude").Value.ToString());
                    baixa.PickupLng = Convert.ToDouble(baixaData.Child("longitude").Value.ToString());
                    baixa.Motivo = baixaData.Child("motivo").Value.ToString();
                    var end = baixaData.Child("endereco").Value ?? "";
                    baixa.Endereco = end.ToString();
                    baixa.Key = baixaData.Key;
                    baixaDetailsList.Add(baixa);
                }
                baixaRetrived.Invoke(this, new baixaDataEventArgs { baixa = baixaDetailsList });
            }
            else
            {
                UserDialogs.Instance.Toast("Você não realizou nenhuma baixa ainda!", TimeSpan.FromSeconds(1));
            }
        }

        public void Create()
        {
            FirebaseDatabase database = AppDataHelper.GetDatabase();
            DatabaseReference getBaixaRef = database.GetReference("users/" + AppDataHelper.GetCurrentUser().Uid + "/Baixas");
            getBaixaRef.AddValueEventListener(this);

        }
        public async void deleteBaixa(string chave)
        {
            DatabaseReference getBaixaRef = AppDataHelper.GetDatabase().GetReference("users/" + AppDataHelper.GetCurrentUser().Uid + "/Baixas/"+chave);
            await getBaixaRef.RemoveValueAsync();
        }
    }
}
﻿using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Widget;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using IntLog.EventListeners;
using IntLog.Model;
using Java.Util;
using System;

namespace IntLog.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/IntLogBaixaTheme", MainLauncher = false)]
    public class RegisterActivity : AppCompatActivity
    {
        private TextInputLayout nameText;
        private TextInputLayout phoneText;
        private TextInputLayout cpfText;
        private TextInputLayout passwordText;
        private TextInputLayout emailText;
        private Button registerButton;
        private CoordinatorLayout rootView;
        private TextView clickToLoginText;

        private FirebaseAuth mAuth;
        private FirebaseDatabase database;

        private User usuario;

        ISharedPreferences preferences =
            Application.Context.GetSharedPreferences("userinfo", FileCreationMode.Private);
        ISharedPreferencesEditor editor;

        TaskCompletionListener taskCompletionListener = new TaskCompletionListener();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.register);
            // Create your application here

            IniciaFirebase();
            mAuth = FirebaseAuth.Instance;
            UserDialogs.Init(this);
            ConnectControl();
            //var userAux = RetriveData();
            //if (userAux.Cpf != null)
            //{
            //    nameText.EditText.Text = userAux.Nome;
            //    phoneText.EditText.Text = userAux.Telefone;
            //    cpfText.EditText.Text = userAux.Cpf;
            //    emailText.EditText.Text = userAux.Email;
            //}
        }

        void ConnectControl()
        {
            nameText = (TextInputLayout)FindViewById(Resource.Id.nameText);
            phoneText = (TextInputLayout)FindViewById(Resource.Id.phoneText);
            phoneText.EditText.AddTextChangedListener(new Mask(phoneText.EditText, "(##) #####-####"));
            registerButton = (Button)FindViewById(Resource.Id.registerButton);
            cpfText = (TextInputLayout)FindViewById(Resource.Id.cpfText);
            cpfText.EditText.AddTextChangedListener(new Mask(cpfText.EditText, "###.###.###-##"));
            emailText = FindViewById<TextInputLayout>(Resource.Id.emailText);
            rootView = (CoordinatorLayout)FindViewById(Resource.Id.rootView);
            passwordText = (TextInputLayout)FindViewById(Resource.Id.senhaText);
            clickToLoginText = (TextView)FindViewById(Resource.Id.clickToLogin);
            clickToLoginText.Click += ClickToLoginText_Click;

            registerButton.Click += RegisterButton_Click;
        }

        private void ClickToLoginText_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(LoginActivity));
            Finish();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {

            usuario = new User(nameText.EditText.Text, phoneText.EditText.Text, cpfText.EditText.Text, emailText.EditText.Text, passwordText.EditText.Text);

            if (usuario.Nome.Length < 3)
            {
                Snackbar.Make(rootView, "Entre com um nome Valido!", Snackbar.LengthShort).Show();
                return;
            }
            else if (usuario.Telefone.Length < 11)
            {
                Snackbar.Make(rootView, "Entre com um número Valido!", Snackbar.LengthShort).Show();
                return;
            }
            else if (usuario.Cpf.Length < 9)
            {
                Snackbar.Make(rootView, "Entre com um cpf Valido!", Snackbar.LengthShort).Show();
                return;
            }
            else if (!usuario.Email.Contains("@"))
            {
                Snackbar.Make(rootView, "Entre com um email Valido!", Snackbar.LengthShort).Show();
                return;
            }
            else if (usuario.Senha.Length < 8)
            {
                Snackbar.Make(rootView, "Entre com uma senha Valida!", Snackbar.LengthShort).Show();
                return;
            }

            UserDialogs.Instance.ShowLoading("Carregando...", MaskType.Black);
            registerUser(usuario);

        }

        void registerUser(User usuario)
        {
            taskCompletionListener.Sucess += TaskCompletionListenerOnSucess;
            taskCompletionListener.Failure += TaskCompletionListenerOnFailure;

            mAuth.CreateUserWithEmailAndPassword(usuario.Email, usuario.Senha)
                .AddOnSuccessListener(this, taskCompletionListener)
                .AddOnFailureListener(this, taskCompletionListener);
        }

        private void TaskCompletionListenerOnFailure(object sender, EventArgs e)
        {
            UserDialogs.Instance.HideLoading();
            string mensagem = default;
            var ex = ((TaskCompletionListener)sender);
            if(ex.Message.Equals("The email address is already in use by another account."))
            {
                mensagem = "Este email já está sendo utilizado.";
            }
            else
            {
                mensagem = ex.Message;
            }
            Snackbar.Make(rootView, $"Usuario não foi registrado. error: {ex.Message}", 20).Show();
        }

        private void TaskCompletionListenerOnSucess(object sender, EventArgs e)
        {

            if (usuario == null)
                throw new Exception("Usuario não criado");

            HashMap userMap = new HashMap();
            userMap.Put("email", usuario.Email);
            userMap.Put("phone", usuario.Telefone);
            userMap.Put("Fullname", usuario.Nome);
            userMap.Put("cpf", usuario.Cpf);
            userMap.Put("password", usuario.Senha);

            var userAux = RetriveData();
            if (userAux.Cpf == null)
            {
                SaveToSharedPreference();
            }
            DatabaseReference userReference = database.GetReference("users/" + mAuth.CurrentUser.Uid);
            userReference.SetValue(userMap);
            UserDialogs.Instance.HideLoading();
            Snackbar.Make(rootView, "Usuario registrado com sucesso.", Snackbar.LengthShort).Show();
            StartActivity(typeof(LoginActivity));
        }

        void SaveToSharedPreference()
        {
            preferences =
                Application.Context.GetSharedPreferences("userinfo", FileCreationMode.Private);
            editor = preferences.Edit();

            editor.PutString("email", usuario.Email);
            editor.PutString("fullname", usuario.Nome);
            editor.PutString("phone", usuario.Telefone);
            editor.PutString("cpf", usuario.Cpf);

            editor.Apply();
        }

        private User RetriveData()
        {
            return new User(preferences.GetString("fullname", null), preferences.GetString("phone", null), preferences.GetString("cpf", null), preferences.GetString("email", null), null);

        }

        void IniciaFirebase()
        {
            FirebaseApp app = FirebaseApp.InitializeApp(this);
            if (app == null)
            {
                var option = new FirebaseOptions.Builder()
                    .SetApplicationId("uber-clone-e8f14")
                    .SetApiKey("AIzaSyC9O5HC7oVuVTwF6WrYbFu3I1KaxxWtBvo")
                    .SetDatabaseUrl(@"https://uber-clone-e8f14.firebaseio.com")
                    .SetStorageBucket("uber-clone-e8f14.appspot.com")
                    .Build();

                app = FirebaseApp.InitializeApp(this, option);
                database = FirebaseDatabase.GetInstance(app);
            }
            else
            {
                database = FirebaseDatabase.GetInstance(app);
            }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace IntLog.Model
{
    public class User
    {
        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public string Telefone
        {
            get => telefone;
            set => telefone = value;
        }

        public string Cpf
        {
            get => cpf;
            set => cpf = value;
        }

        public string Email
        {
            get => email;
            set => email = value;
        }

        public string Senha
        {
            get => senha;
            set => senha = value;
        }

        public User(string nome, string telefone, string cpf, string email, string senha)
        {
            this.Nome = nome; //?? throw new ArgumentNullException(nameof(nome));
            this.Telefone = telefone; //?? throw new ArgumentNullException(nameof(telefone));
            this.Cpf = cpf; //?? throw new ArgumentNullException(nameof(cpf));
            this.Email = email; //?? throw new ArgumentNullException(nameof(email));
            this.Senha = senha; //?? throw new ArgumentNullException(nameof(senha));
        }

        private string nome;
        private string telefone;
        private string cpf;
        private string email;
        private string senha;

    }
}
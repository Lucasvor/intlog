﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace IntLog.Helpers
{
    public class BottomSheetToolbarToggleCallback : BottomSheetBehavior.BottomSheetCallback
    {

        private Android.Support.V7.App.ActionBar actionBar;

        public BottomSheetToolbarToggleCallback(Android.Support.V7.App.ActionBar actionBar)
        {
            if (actionBar != null)
                this.actionBar = actionBar;
            else
                throw new Exception("Tollbar is null");
        }

        public override void OnSlide(View bottomSheet, float slideOffset)
        {
            
        }

        public override void OnStateChanged(View bottomSheet, int newState)
        {
            switch (newState)
            {
                case BottomSheetBehavior.StateExpanded:
                    actionBar.SetDisplayHomeAsUpEnabled(false);
                    break;
                case BottomSheetBehavior.StateHidden:
                    actionBar.SetDisplayHomeAsUpEnabled(true);
                    break;
                case BottomSheetBehavior.StateSettling:
                    actionBar.SetDisplayHomeAsUpEnabled(true);
                    break;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using IntLog.Model;

namespace IntLog.Helpers
{
    static class AppDataHelper
    {
       static ISharedPreferences preferences = Application.Context.GetSharedPreferences("userinfo", FileCreationMode.Private);
        public static FirebaseDatabase GetDatabase()
        {
            var app = FirebaseApp.InitializeApp(Application.Context);
            FirebaseDatabase database;

            if (app == null)
            {
                var option = new FirebaseOptions.Builder()
                    .SetApplicationId("uber-clone-e8f14")
                    .SetApiKey("AIzaSyC9O5HC7oVuVTwF6WrYbFu3I1KaxxWtBvo")
                    .SetDatabaseUrl(@"https://uber-clone-e8f14.firebaseio.com")
                    .SetStorageBucket("uber-clone-e8f14.appspot.com")
                    .Build();

                app = FirebaseApp.InitializeApp(Application.Context, option);
                database = FirebaseDatabase.GetInstance(app);
            }
            else
            {
                database = FirebaseDatabase.GetInstance(app);
            }

            return database;
        }

        public static FirebaseAuth GetFirebaseAuth()
        {
            var app = FirebaseApp.InitializeApp(Application.Context);
            FirebaseAuth mAuth;
            if (app == null)
            {
                var option = new FirebaseOptions.Builder()
                    .SetApplicationId("uber-clone-e8f14")
                    .SetApiKey("AIzaSyC9O5HC7oVuVTwF6WrYbFu3I1KaxxWtBvo")
                    .SetDatabaseUrl(@"https://uber-clone-e8f14.firebaseio.com")
                    .SetStorageBucket("uber-clone-e8f14.appspot.com")
                    .Build();

                app = FirebaseApp.InitializeApp(Application.Context, option);
                mAuth = FirebaseAuth.Instance;
            }
            else
            {
                mAuth = FirebaseAuth.Instance;
            }

            return mAuth;
        }

        public static FirebaseUser GetCurrentUser()
        {
            var app = FirebaseApp.InitializeApp(Application.Context);
            FirebaseAuth mAuth;
            FirebaseUser mUser;
            if (app == null)
            {
                var option = new FirebaseOptions.Builder()
                    .SetApplicationId("uber-clone-e8f14")
                    .SetApiKey("AIzaSyC9O5HC7oVuVTwF6WrYbFu3I1KaxxWtBvo")
                    .SetDatabaseUrl(@"https://uber-clone-e8f14.firebaseio.com")
                    .SetStorageBucket("uber-clone-e8f14.appspot.com")
                    .Build();

                app = FirebaseApp.InitializeApp(Application.Context, option);
                mAuth = FirebaseAuth.Instance;
                mUser = mAuth.CurrentUser;
            }
            else
            {
                mAuth = FirebaseAuth.Instance;
                mUser = mAuth.CurrentUser;
            }

            return mUser;
        }
        public static User GetUser()
        {
            var reference = AppDataHelper.GetDatabase().GetReference("users/" + AppDataHelper.GetCurrentUser().Uid);
            var fullname = reference.Child("Fullname");
            var cpf = reference.Child("cpf");
            var email = reference.Child("email");
            var phone = reference.Child("phone");

            return null;
        }

        public static string GetFullName()
        {
            return preferences.GetString("Fullname", "");
        }

        public static string GetEmail()
        {
            return preferences.GetString("email", "");
        }
        public static string GetPhone()
        {
            return preferences.GetString("phone", "");
        }

        public static string GetCPF()
        {
            return preferences.GetString("cpf", "");
        }
        public static bool IsLogin()
        {
            return preferences.GetBoolean("login", false);
        }
    }
}
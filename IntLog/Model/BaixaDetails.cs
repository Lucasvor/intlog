﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace IntLog.Model
{
    public class BaixaDetails
    {
        public DateTime DateTime { get; set; }
        public double PickupLat { get; set; }
        public double PickupLng { get; set; }
        public string IRL { get; set; }
        public string Motivo { get; set; }
        public string Key { get; set; }
        public string Endereco { get; set; }

        public string getAll()
        {
            return string.Join(", ",Endereco,IRL,Motivo,PickupLat.ToString(),PickupLng.ToString(),DateTime.ToString());
        }


    }
    public class BaixaList
    {
        //static BaixaDetails[] listBaixa =
        //{
        //    new BaixaDetails(){DateTime = DateTime.Now, IRL ="1231241234",Motivo = "motivo 1"},
        //    new BaixaDetails(){DateTime = DateTime.Now, IRL ="2131242531",Motivo = "motivo 2"},
        //    new BaixaDetails(){DateTime = DateTime.Now, IRL ="12344125412",Motivo = "motivo 3"},
        //    new BaixaDetails(){DateTime = DateTime.Now, IRL ="5123415123",Motivo = "motivo 4"},
        //    new BaixaDetails(){DateTime = DateTime.Now, IRL ="512312341",Motivo = "motivo 5"},
        //    new BaixaDetails(){DateTime = DateTime.Now, IRL ="5412312312",Motivo = "motivo 6"},
        //    new BaixaDetails(){DateTime = DateTime.Now, IRL ="12312451",Motivo = "motivo 7"},
        //    new BaixaDetails(){DateTime = DateTime.Now, IRL ="512312512",Motivo = "motivo 8"},
        //    new BaixaDetails(){DateTime = DateTime.Now, IRL ="512312312",Motivo = "motivo 9"},

        //};

        private List<BaixaDetails> baixas;
        //Random random;
        //public BaixaList()
        //{
        //    this.baixas = listBaixa;
        //    random = new Random();
        //}
        public BaixaList(List<BaixaDetails> list)
        {
            this.baixas = list;
        }
        public void Reverse()
        {
            baixas.Reverse();
        }

        public int numBaixas
        {
            get
            {
                return baixas.Count;
            }
        }
        public BaixaDetails this[int i]
        {
            get { return baixas[i]; }
        }
    }

    public class BaixaViewHolder : RecyclerView.ViewHolder
    {
        public TextView Datetime { get; set; }
        public TextView Time { get; set; }
        public TextView Endereco { get; set; }
        public TextView IRL { get; set; }
        public TextView Motivo { get; set; }
        public ImageView Delete { get; set; }

        public BaixaViewHolder(View itemView, Action<ClickEventArgs> clickListener,Action<ClickEventArgs> longClickListener, Action<ClickEventArgs> deleteClickListener) : base(itemView)
        {
            Datetime = itemView.FindViewById<TextView>(Resource.Id.dateTimeTextView);
            Time = itemView.FindViewById<TextView>(Resource.Id.timeTextView);
            IRL = itemView.FindViewById<TextView>(Resource.Id.IrlTextView);
            Endereco = itemView.FindViewById<TextView>(Resource.Id.EnderecoTextView);
            Motivo = itemView.FindViewById<TextView>(Resource.Id.motivoHistTextView);
            Delete = itemView.FindViewById<ImageView>(Resource.Id.excluiBaixaHistorico);

            itemView.Click += (sender, e) => clickListener(new ClickEventArgs { View = itemView, Position = AdapterPosition });
            ItemView.LongClick += (sender, e) => longClickListener(new ClickEventArgs { View = ItemView, Position = AdapterPosition });
            Delete.Click += (sender, e) => deleteClickListener(new ClickEventArgs { View = itemView, Position = AdapterPosition });
        }

    }
    public class BaixaListAdapter : RecyclerView.Adapter
    {
        public event EventHandler<ClickEventArgs> ItemClick;
        public event EventHandler<ClickEventArgs> ItemLongClick;
        public event EventHandler<ClickEventArgs> DeleteItemClick;
        public BaixaList baixaList;
        public override int ItemCount => baixaList.numBaixas;

        public BaixaListAdapter(BaixaList baixaList)
        {
            this.baixaList = baixaList;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            BaixaViewHolder bh = holder as BaixaViewHolder;
            bh.Datetime.Text = this.baixaList[position].DateTime.ToString("dd/MM/yyyy");
            bh.Time.Text = this.baixaList[position].DateTime.ToString("HH:mm");
            bh.IRL.Text =  "IRL: " +this.baixaList[position].IRL;
            bh.Motivo.Text = this.baixaList[position].Motivo;
            bh.Endereco.Text = this.baixaList[position].Endereco;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ItemRow, parent, false);
            BaixaViewHolder bh = new BaixaViewHolder(itemView, OnClick, OnLongClick,OnDeleteClick);
            return bh;
        }
        private void OnClick(ClickEventArgs args) => ItemClick?.Invoke(this, args);
        private void OnLongClick(ClickEventArgs args) => ItemLongClick?.Invoke(this, args);
        private void OnDeleteClick(ClickEventArgs args) => DeleteItemClick?.Invoke(this, args);


    }

    public class ClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}
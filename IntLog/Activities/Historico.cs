﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using IntLog.EventListeners;
using IntLog.Model;

namespace IntLog.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/IntLogBaixaTheme", MainLauncher = false, NoHistory = true)]
    public class Historico : Activity
    {
        private RecyclerView recyclerView;
        private RecyclerView.LayoutManager layoutManager;
        private BaixaList baixaList;
        private BaixaListAdapter baixaListAdapter;
        private BaixaEventListener baixaEventListener;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //baixaList = new BaixaList();
            
            SetContentView(Resource.Layout.Historico);
            recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            layoutManager = new LinearLayoutManager(this);
            //recyclerView.SetLayoutManager(layoutManager);
            RetriveData();
            UserDialogs.Init(this);
            //SetRecyclerAdaptor();
            //baixaListAdapter = new BaixaListAdapter(baixaList);
            //baixaListAdapter.ItemClick += BaixaListAdapter_ItemClick;
            //recyclerView.SetAdapter(baixaListAdapter);
            // Create your application here
        }

        private void RelativeLayoutExcluiBaixa_Click(object sender, EventArgs e)
        {
            Toast.MakeText(this, "Você irá excluir o item: ", ToastLength.Long).Show();
        }

        public void RetriveData()
        {
            baixaEventListener = new BaixaEventListener();
            baixaEventListener.Create();
            baixaEventListener.baixaRetrived += BaixaEventListener_baixaRetrived;
        }

        private void BaixaEventListener_baixaRetrived(object sender, BaixaEventListener.baixaDataEventArgs e)
        {
            baixaList = new BaixaList(e.baixa);
            baixaList.Reverse();
            SetRecyclerAdaptor();
        }

        private void BaixaListAdapter_ItemClick(object sender, ClickEventArgs e)
        {
            Toast.MakeText(this, "esse é o item: " + (e.Position + 1), ToastLength.Long).Show();
        }

        protected override void OnResume()
        {
            base.OnResume();
           // SetRecyclerAdaptor();
        }

        private void SetRecyclerAdaptor()
        {
            baixaListAdapter = new BaixaListAdapter(baixaList);
            
            recyclerView.SetLayoutManager(layoutManager);
            baixaListAdapter.ItemClick += BaixaListAdapter_ItemClick;
            baixaListAdapter.ItemLongClick += BaixaListAdapter_ItemLongClick;
            baixaListAdapter.DeleteItemClick += BaixaListAdapter_DeleteItemClick;
            recyclerView.SetAdapter(baixaListAdapter);

            if(baixaListAdapter.ItemCount == 0)
            {
                Snackbar.Make(recyclerView,"Você não realizou nenhuma baixa ainda.", 5);
            }
        }

        private async void BaixaListAdapter_DeleteItemClick(object sender, ClickEventArgs e)
        {
            var chave = baixaList[e.Position].Key;
            var respota = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig().SetTitle("IntLog - Historico").SetMessage("Deseja Realmente excluir esse item").SetCancelText("Não").SetOkText("Sim"));
            if (respota)
            {
                baixaEventListener.deleteBaixa(chave);
                UserDialogs.Instance.Toast("Você confirmou a exclusão!!", TimeSpan.FromSeconds(3));
            }
            else
            {
                
                UserDialogs.Instance.Toast("você cancelou a exclusão!!", TimeSpan.FromSeconds(3));
            }
        }

        private void BaixaListAdapter_ItemLongClick(object sender, ClickEventArgs e)
        {

            UserDialogs.Instance.Toast("Clique longo!", TimeSpan.FromSeconds(3));

            //var dialog = new AlertDialog.Builder(this);
            //AlertDialog alert = dialog.Create();
            //alert.SetTitle("IntLog - Historico");
            //alert.SetMessage("Deseja realmente excluir esse item?");
            //alert.SetIcon(Resource.Mipmap.ic_delete);
            //alert.SetButton("Sim", (c, ev) =>
            //{
            //    UserDialogs.Instance.Toast("Você confirmou a exclusão!!", TimeSpan.FromSeconds(3));

            //});
            //alert.SetButton2("Não", (c, ev) => { UserDialogs.Instance.Toast("você cancelou a exclusão!!", TimeSpan.FromSeconds(3)); });
            //alert.Show();

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Common.Apis;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using IntLog.EventListeners;
using IntLog.Model;
using ZXing.Mobile;

namespace IntLog.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/IntLogBaixaTheme", MainLauncher = false, NoHistory = true)]
    public class ZxingActivity : Activity
    {
        MobileBarcodeScanner scanner;
        private CreateRequestEventListener requestEventListener;
        private string motivo, latitude, longitude, endereco;

        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //SetContentView(Resource.Layout.layoutZxing);

            scanner = new MobileBarcodeScanner();
            MobileBarcodeScanner.Initialize(Application);



            //motivoTextView = FindViewById<TextView>(Resource.Id.motivoText);
            //motivoTextView.Text = Intent.GetStringExtra("Motivo") ?? "Informação não disponivel";
            //buttonScanDefaultView = this.FindViewById<Button>(Resource.Id.buttonScanDefaultView);
            Button flashButton;
            View zxingOverlay;
            TextView motivoTextView;
            //    buttonScanDefaultView.Click += async delegate
            //    {

            //        //Tell our scanner to use the default overlay
            //        //scanner.UseCustomOverlay = false;

            //        ////We can customize the top and bottom text of the default overlay
            //        //scanner.TopText = "Hold the camera up to the barcode\nAbout 6 inches away";
            //        //scanner.BottomText = "Wait for the barcode to automatically scan!";

            //        ////Start scanning
            //        //var result = await scanner.Scan();

            //        //HandleScanResult(result);


            //        scanner.UseCustomOverlay = true;

            //        //Inflate our custom overlay from a resource layout
            //        zxingOverlay = LayoutInflater.FromContext(this).Inflate(Resource.Layout.ZxingOverlay, null);

            //        //Find the button from our resource layout and wire up the click event
            //        flashButton = zxingOverlay.FindViewById<Button>(Resource.Id.buttonZxingFlash);
            //        flashButton.Click += (sender, e) => scanner.ToggleTorch();

            //        //Set our custom overlay
            //        scanner.CustomOverlay = zxingOverlay;

            //        //Start scanning!
            //        var result = await scanner.Scan(new MobileBarcodeScanningOptions { AutoRotate = true });

            //        HandleScanResult(result);

            //    };
            //}
            scanner.UseCustomOverlay = true;

            //Inflate our custom overlay from a resource layout
            zxingOverlay = LayoutInflater.FromContext(this).Inflate(Resource.Layout.ZxingOverlay, null);

            //Find the button from our resource layout and wire up the click event
            flashButton = zxingOverlay.FindViewById<Button>(Resource.Id.buttonZxingFlash);
            flashButton.Click += (sender, e) => scanner.ToggleTorch();

            motivoTextView = zxingOverlay.FindViewById<TextView>(Resource.Id.MotivoTextView);
            motivo = Intent.GetStringExtra("Motivo") ?? "Informação não disponivel";
            latitude = Intent.GetStringExtra("latitude") ?? "";
            longitude = Intent.GetStringExtra("longitude") ?? "";
            endereco = Intent.GetStringExtra("endereco") ?? "";
            motivoTextView.Text = endereco + "\n" + motivo;
            if (latitude == "" && longitude == "" && endereco == "")
            {
                this.RunOnUiThread(() => Toast.MakeText(this, "Consulta de LAT/LNG não trouxe resultado.", ToastLength.Short).Show());
            }
            //Set our custom overlay
            scanner.CustomOverlay = zxingOverlay;

            //Start scanning!
            var result = await scanner.Scan(new MobileBarcodeScanningOptions { AutoRotate = true });
            //Finish();
            //Intent data = new Intent();
            //data.PutExtra("Barcode", result.Text);
            //SetResult(CommonStatusCodes.Success, data);
            //Toast.MakeText(this, result.Text, ToastLength.Short).Show();

            HandleScanResult(result);
        }

        protected override void OnStop()
        {

            Finish();
            base.OnStop();
        }

        void HandleScanResult(ZXing.Result result)
        {
            BaixaDetails baixa = null;
            string msg = "";

            if (result != null && !string.IsNullOrEmpty(result.Text))
            {
                msg = result.Text;
                baixa = new BaixaDetails()
                {
                    DateTime = DateTime.Now,
                    IRL = msg,
                    Motivo = motivo,
                    PickupLat = Convert.ToDouble(latitude),
                    PickupLng = Convert.ToDouble(longitude),
                    Endereco = endereco
                    
                };
                requestEventListener = new CreateRequestEventListener(baixa);
                requestEventListener.CreateRequest();
            }
            else
                msg = "Consulta foi cancelada!";
            if (baixa != null)
                this.RunOnUiThread(() => Toast.MakeText(this, baixa.getAll(), ToastLength.Short).Show());
            else
                this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());

            //this.RunOnUiThread((delegate {
            //    Intent data = new Intent();
            //    data.PutExtra("Barcode", msg);
            //    SetResult(CommonStatusCodes.Success,data);
            //    //Finish();
            //    //Toast.MakeText(this, msg + "sdasdasd", ToastLength.Short).Show();
            //    //Toast.MakeText(this, msg + "sdasdasd", ToastLength.Short).Show();
            //}));

        }
        //protected override void OnResume()
        //{
        //    base.OnResume();

        //    if (ZXing.Net.Mobile.Android.PermissionsHandler.NeedsPermissionRequest(this))
        //        ZXing.Net.Mobile.Android.PermissionsHandler.RequestPermissionsAsync(this);
        //}
        //public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        //{
        //    global::ZXing.Net.Mobile.Android.PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        //}
    }

}
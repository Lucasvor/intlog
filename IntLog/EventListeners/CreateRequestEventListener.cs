﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using IntLog.Helpers;
using IntLog.Model;
using Java.Util;

namespace IntLog.EventListeners
{
    class CreateRequestEventListener : Java.Lang.Object, IValueEventListener
    {
        private BaixaDetails baixaDetails;
        private FirebaseDatabase database;
        private DatabaseReference baixaRef;
        public void OnCancelled(DatabaseError error)
        {

        }

        public void OnDataChange(DataSnapshot snapshot)
        {

        }

        public CreateRequestEventListener(BaixaDetails baixaDetails)
        {
            this.baixaDetails = baixaDetails;
            database = AppDataHelper.GetDatabase();
        }

        public void CreateRequest()
        {
            baixaRef = database.GetReference("users/" + AppDataHelper.GetCurrentUser().Uid + "/Baixas").Push();

            HashMap baixa = new HashMap();
            baixa.Put("latitude", baixaDetails.PickupLat);
            baixa.Put("longitude", baixaDetails.PickupLng);
            baixa.Put("irl", baixaDetails.IRL);
            baixa.Put("datetime", baixaDetails.DateTime.ToString());
            baixa.Put("motivo", baixaDetails.Motivo);
            baixa.Put("endereco", baixaDetails.Endereco);

            baixaRef.AddValueEventListener(this);
            baixaRef.SetValue(baixa);


        }
    }
}
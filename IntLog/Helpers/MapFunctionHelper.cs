﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Gms.Maps.Model;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using IntLog.HelpersA;
using Newtonsoft.Json;
using Xamarin.Essentials;

namespace IntLog.Helpers
{

    public class MapFunctionHelper
    {
        public string mapkey;
        Geocoder geocoder;

        public MapFunctionHelper(string mapKey)
        {
            this.mapkey = mapKey;
        }
        public MapFunctionHelper(string mapKey,Geocoder geocoder)
        {
            this.mapkey = mapKey;
            this.geocoder = geocoder;

        }

        public string GetGeoCodeUrl(double lat, double lng)
        {
            string url = $"https://maps.googleapis.com/maps/api/geocode/json?latlng={lat.ToString(CultureInfo.InvariantCulture)},{lng.ToString(CultureInfo.InvariantCulture)}&key={mapkey}";
            return url;
        }

        public async Task<string> GetGeoJsonAsync(string url)
        {
            var handler = new HttpClientHandler();
            HttpClient client = new HttpClient(handler);
            string result = await client.GetStringAsync(url);
            return result;
        }

        public async Task<string> FindCordinateAddress(LatLng position)
        {
            //addresses = geocoder.GetFromLocation(position.Latitude, position.Longitude, 1).ToList();
            var placemarks = await Geocoding.GetPlacemarksAsync(position.Latitude, position.Longitude);
            string url = GetGeoCodeUrl(position.Latitude, position.Longitude);
            string json = "";
            string placeAddress = "";
            
            
            //Check for internet Connection

            json = await GetGeoJsonAsync(url);

            if (!string.IsNullOrEmpty(json))
            {
                var geoCodeData = JsonConvert.DeserializeObject<GeocodingParser>(json);
                if (!geoCodeData.status.Contains("ZERO"))
                {
                    if(geoCodeData.results[0] != null)
                    {
                        placeAddress = geoCodeData.results[0].formatted_address;
                    }
                }
            }
            return placeAddress;
        }
    }

}
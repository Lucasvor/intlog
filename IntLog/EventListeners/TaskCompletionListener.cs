﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Tasks;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace IntLog.EventListeners
{
    public class TaskCompletionListener : Java.Lang.Object, IOnSuccessListener, IOnFailureListener,IOnCompleteListener
    {
        public event EventHandler Sucess;
        public event EventHandler Failure;
        public event EventHandler Complete;
        public string Message { get; set; }
        public int Status { get; set; }
        //1 pega posição atual/ 2 para baixa / 3 paara aparecer no mapa

        public void OnFailure(Java.Lang.Exception e)
        {
            Message = e.Message.Equals("ERROR_WRONG_PASSWORD") ? "Senha errada!" : e.Message;
            Failure?.Invoke(this, new EventArgs());
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            Sucess?.Invoke(this, new EventArgs());
        }

        public void OnComplete(Task task)
        {
            Complete?.Invoke(task,new EventArgs());
        }
    }
}
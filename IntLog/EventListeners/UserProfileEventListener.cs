﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using IntLog.Helpers;
using IntLog.Model;

namespace IntLog.EventListeners
{
    public class UserProfileEventListener : Java.Lang.Object, IValueEventListener
    {
        private ISharedPreferences preferences = Application.Context.GetSharedPreferences("userinfo",FileCreationMode.Private);
        private ISharedPreferencesEditor editor;
        private User user;
        public event EventHandler<GetUserInfo> userEvent;


        public class GetUserInfo : EventArgs
        {
            public User user { get; set; }
        }
        public void OnCancelled(DatabaseError error)
        {
            
        }

        public void OnDataChange(DataSnapshot snapshot)
        {
            if (snapshot.Value != null)
            {
                
                string fullname, email, cpf, phone;
                fullname = (snapshot.Child("Fullname") != null) ? snapshot.Child("Fullname").Value.ToString() : "";
                email = (snapshot.Child("email") != null) ? snapshot.Child("email").Value.ToString() : "";
                phone = (snapshot.Child("phone") != null) ? snapshot.Child("phone").Value.ToString() : "";
                cpf = (snapshot.Child("cpf") != null) ? snapshot.Child("cpf").Value.ToString() : "";

                user = new User(fullname,phone,cpf,email,null);
                //if (snapshot.Child("phone") != null)
                //{
                //    phone = snapshot.Child("phone").Value.ToString();
                //}

                //if (snapshot.Child("cpf") != null)
                //{
                //    cpf = snapshot.Child("cpf").Value.ToString();

                //}

                editor.PutString("Fullname", fullname);
                editor.PutString("email", email);
                editor.PutString("phone", phone);
                editor.PutString("cpf", cpf);
                editor.Apply();
                
                userEvent.Invoke(this, new GetUserInfo { user = this.user });
            }
        }

        public void Create()
        {
            editor = preferences.Edit();
            FirebaseDatabase database = AppDataHelper.GetDatabase();
            string userId = AppDataHelper.GetCurrentUser().Uid;
            DatabaseReference profileReference = database.GetReference($"users/{userId}");
            profileReference.AddValueEventListener(this);
        }
        public User GetUser()
        {
            if (user != null)
                return user;
            else
                return null;
        }
    }
}
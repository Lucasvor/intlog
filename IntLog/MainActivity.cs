﻿using System;
using System.Threading;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Locations;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using IntLog.Activities;
using IntLog.EventListeners;
using IntLog.Helpers;
using IntLog.Model;
using Task = Android.Gms.Tasks.Task;
using Android.Graphics;
using Acr.UserDialogs;
using System.Linq;
using System.Threading.Tasks;

namespace IntLog
{
    [Activity(Label = "@string/app_name", Theme = "@style/IntLogBaixaTheme", MainLauncher = false)]
    public class MainActivity : AppCompatActivity, IOnMapReadyCallback
    {

        //Firebase
        UserProfileEventListener profileEventListener = new UserProfileEventListener();


        Android.Support.V7.Widget.Toolbar mainToolbar;
        Android.Support.V7.App.ActionBar actionBar;
        private Android.Support.V4.Widget.DrawerLayout drawerLayout;
        private GoogleMap mainMap;
        protected const int REQUEST_CHECK_SETTINGS = 0x1;


        private CoordinatorLayout rootview;

        private RelativeLayout meuLocalButton;

        //Bottomsheet
        private BottomSheetBehavior confirmaDetalheSheetBehavior;

        private ImageView iconBaixo;

        //Buttons
        private Button fazerBaixaButton;
        //private Button confirmaBaixaButton;

        //TextView
        private TextView endTextView;
        private TextView accountTitle;
        private TextView accountCpf;
        private TextView accountEmail;
        private TextView accountPhone;

        private BottomSheetListView motivoBaixaListView;
        private ArrayAdapter<string> listAdapter;

        private NavigationView navigationView;
        // grupo de permissões para solicitar permissão
        private readonly string[] permissonGroupLocation =
            {Manifest.Permission.AccessCoarseLocation, Manifest.Permission.AccessFineLocation};
        //tipo de permissão
        private const int requestLocationId = default;

        private LocationRequest mLocationRequest;
        private FusedLocationProviderClient locationClient;
        private Android.Locations.Location mLastLocation;

        private static int UPDATE_INTERVAl = 5; // 5 segundos
        private static int FASTEST_INTERVAL = 5;
        private static int DISPLACEMENT = 3; //meters

        //LATLNG
        LatLng latLng;
        User user;
        bool estaEmUsoLocalizaGPS;
        // Task para os resultados da distancia e GPS
        TaskCompletionListener taskCompletionListener;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            ConnectControl();

            SupportMapFragment mapFragment =
                (SupportMapFragment)SupportFragmentManager.FindFragmentById(Resource.Id.map);
            mapFragment.GetMapAsync(this);

            listAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItemActivated1, Resources.GetStringArray(Resource.Array.motivoBaixa_array));
            motivoBaixaListView.Adapter = listAdapter;
            motivoBaixaListView.TextFilterEnabled = true;
            motivoBaixaListView.ItemClick += MotivoBaixaListView_ItemClick;

            UserDialogs.Init(this);
            profileEventListener.Create();
            profileEventListener.userEvent += ProfileEventListener_userEvent;
            CheckLocationPermission();

            //Teste



            //LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
            //    .AddLocationRequest(mLocationRequest);
            //settingsBuilder.SetAlwaysShow(true);
            //TaskCompletionListener taskCompletionListener = new TaskCompletionListener();
            //taskCompletionListener.Complete += TaskCompletionListenerOnComplete;

            //LocationManager locationManager = (LocationManager)GetSystemService(Context.LocationService);
            //CriaChamadodeLocalizacao();
            //if (!locationManager.IsProviderEnabled(LocationManager.GpsProvider))
            //{
            //    taskCompletionListener.Status = 3;
            //    Task result = LocationServices.GetSettingsClient(this).CheckLocationSettings(settingsBuilder.Build());

            //    result.AddOnCompleteListener(this, taskCompletionListener);

            //}
            //else
            //{
            //    PegaMinhaLocalizacaoAsync(15);
            //}


        }

        private void ProfileEventListener_userEvent(object sender, UserProfileEventListener.GetUserInfo e)
        {
            var user = e.user;
            if(user != null)
            {
                this.user = user;
            }
        }

        private async void MotivoBaixaListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                //Toast.MakeText(this,((TextView)e.View).Text,ToastLength.Long).Show();

                //TEla cheia


                confirmaDetalheSheetBehavior.State = BottomSheetBehavior.StateHidden;
                if (!CheckInternet())
                {
                    UserDialogs.Instance.Toast("Você não está conectado!", TimeSpan.FromSeconds(2));
                    return;
                }

                if (estaEmUsoLocalizaGPS)
                {
                    UserDialogs.Instance.Toast("Serviço está buscando o endereço.", TimeSpan.FromSeconds(1));
                    return;
                }

                estaEmUsoLocalizaGPS = true;

                var intent = new Intent(this, typeof(IntLog.Activities.ZxingActivity));
                intent.PutExtra("Motivo", ((TextView)e.View).Text);
                var minhaPosicao = await VerificaGPS(2);

                if(minhaPosicao != null)
                {
                    intent.PutExtra("latitude", mLastLocation.Latitude.ToString());
                    intent.PutExtra("longitude", mLastLocation.Longitude.ToString());
                    var placemarks = await Xamarin.Essentials.Geocoding.GetPlacemarksAsync(minhaPosicao.Latitude, minhaPosicao.Longitude);
                    var endereco = placemarks?.FirstOrDefault();
                    if (endereco != null)
                    {
                        intent.PutExtra("endereco", $"{endereco.Thoroughfare}, {endereco.SubThoroughfare}, {endereco.SubLocality}");
                        endTextView.Text = $"{endereco.Thoroughfare}, {endereco.SubThoroughfare}, {endereco.SubLocality}";
                    }
                    else
                    {
                        throw new Exception("Não foi possível localizar o endereço.");
                    }

                    StartActivity(intent);
                }


                //mLastLocation = await locationClient.GetLastLocationAsync();
                //if (mLastLocation != null)
                //{
                //    LatLng minhaPosicao = new LatLng(mLastLocation.Latitude, mLastLocation.Longitude);
                //    intent.PutExtra("latitude", mLastLocation.Latitude.ToString());
                //    intent.PutExtra("longitude", mLastLocation.Longitude.ToString());

                //    //Endereco
                //    var placemarks = await Xamarin.Essentials.Geocoding.GetPlacemarksAsync(minhaPosicao.Latitude, minhaPosicao.Longitude);
                //    var endereco = placemarks?.FirstOrDefault();
                //    if (endereco != null)
                //    {
                //        intent.PutExtra("endereco",$"{endereco.Thoroughfare}, {endereco.SubThoroughfare}, {endereco.SubLocality}");
                //        endTextView.Text = $"{endereco.Thoroughfare}, {endereco.SubThoroughfare}, {endereco.SubLocality}";
                //    }
                //    //
                //    mainMap.MoveCamera(CameraUpdateFactory.NewLatLngZoom(minhaPosicao, 20));
                //    StartActivity(intent);
                //}
                //else
                //{
                //    throw new Exception("Não foi possível pegar a localização.");
                //}



                //StartActivity(typeof(Activities.FragmentActivity));

                //meia tela;
                //StartActivity(typeof(Activities.FragmentActivity));
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
            }
        }

        void ConnectControl()
        {
            rootview = FindViewById<CoordinatorLayout>(Resource.Id.rootView);
            navigationView = FindViewById<NavigationView>(Resource.Id.navView);
            navigationView.NavigationItemSelected += NavigationView_NavigationItemSelected;
            drawerLayout = FindViewById<Android.Support.V4.Widget.DrawerLayout>(Resource.Id.drawerLayout);
            mainToolbar = (Android.Support.V7.Widget.Toolbar)FindViewById(Resource.Id.mainToolbar);
            SetSupportActionBar(mainToolbar);
            SupportActionBar.Title = "";
            actionBar = SupportActionBar;
            actionBar.SetHomeAsUpIndicator(Resource.Mipmap.ic_menu_action);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            

            meuLocalButton = FindViewById<RelativeLayout>(Resource.Id.minhaLocalizacao);
            meuLocalButton.Click += MeuLocalButton_Click;
            fazerBaixaButton = FindViewById<Button>(Resource.Id.fazerBaixaButton);
            fazerBaixaButton.Click += FazerBaixaButton_Click;
            endTextView = FindViewById<TextView>(Resource.Id.endTextView);



            //accountTitle.Text = AppDataHelper.GetFullName();
           // var user = AppDataHelper.GetUser();

            FrameLayout canfirmaDetalhesView = (FrameLayout)FindViewById(Resource.Id.confirmaDetalhes_bottomsheet);
            confirmaDetalheSheetBehavior = BottomSheetBehavior.From(canfirmaDetalhesView);
            confirmaDetalheSheetBehavior.SetBottomSheetCallback(new BottomSheetToolbarToggleCallback(actionBar));
            iconBaixo = FindViewById<ImageView>(Resource.Id.IconBaixo);
            iconBaixo.Click += (sender, e) =>
            {
                confirmaDetalheSheetBehavior.State = BottomSheetBehavior.StateHidden;
                
            };
            motivoBaixaListView = FindViewById<BottomSheetListView>(Resource.Id.listMotivosBaixas);
        }
       
        private void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            switch (e.MenuItem.ItemId)
            {
                case (Resource.Id.navHistory):
                    //var toast = Toast.MakeText(this, "Você clico na aba de Historico", ToastLength.Long);
                    //toast.View.Background.SetColorFilter(Color.ParseColor("#3498db"), PorterDuff.Mode.SrcIn);
                    //toast.Show();
                    UserDialogs.Instance.ShowLoading("Carregando Historico", MaskType.Gradient);
                    StartActivity(typeof(Historico));
                    drawerLayout.CloseDrawer(GravityCompat.Start);
                    UserDialogs.Instance.HideLoading();
                    break;
                case (Resource.Id.navSair):
                    Intent intent = new Intent(this, typeof(LoginActivity));
                    StartActivity(intent);
                    var preferences =
                    Application.Context.GetSharedPreferences("userinfo", FileCreationMode.Private);
                    var editor = preferences.Edit();
                    editor.PutBoolean("login", true);
                    editor.Commit();
                    this.Finish();
                    break;
            }
        }

        void ConfirmaButtonSet()
        {
            if (fazerBaixaButton.Visibility == ViewStates.Visible)
                fazerBaixaButton.Visibility = ViewStates.Invisible;
            else
                fazerBaixaButton.Visibility = ViewStates.Visible;

            //if (confirmaBaixaButton.Visibility == ViewStates.Invisible)
            //    confirmaBaixaButton.Visibility = ViewStates.Visible;
            //else
            //    confirmaBaixaButton.Visibility = ViewStates.Invisible;
        }

        private void ConfirmaBaixaButton_Click(object sender, EventArgs e)
        {
            //ConfirmaButtonSet();
            confirmaDetalheSheetBehavior.State = BottomSheetBehavior.StateHidden;

        }

        private void FazerBaixaButton_Click(object sender, EventArgs e)
        {
            //ConfirmaButtonSet();
            confirmaDetalheSheetBehavior.State = BottomSheetBehavior.StateExpanded;
        }

        private async void MeuLocalButton_Click(object sender, EventArgs e)
        {
            //CriaChamadodeLocalizacao();
            //LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
            //    .AddLocationRequest(mLocationRequest);
            //settingsBuilder.SetAlwaysShow(true);
            //TaskCompletionListener taskCompletionListener = new TaskCompletionListener();
            //taskCompletionListener.Complete += TaskCompletionListenerOnComplete;

            //LocationManager locationManager = (LocationManager)GetSystemService(Context.LocationService);
            //if (!locationManager.IsProviderEnabled(LocationManager.GpsProvider))
            //{


            //    Task result = LocationServices.GetSettingsClient(this).CheckLocationSettings(settingsBuilder.Build());
            //    result.AddOnCompleteListener(this, taskCompletionListener);

            //}
            //else
            //{
            //   PegaMinhaLocalizacaoAsync(20);
            //}
            if (estaEmUsoLocalizaGPS)
            {
                UserDialogs.Instance.Toast("Serviço está buscando o endereço.", TimeSpan.FromSeconds(1));
                return;
            }

            estaEmUsoLocalizaGPS = true;
            await VerificaGPS(1);
        }

        private async Task<LatLng> VerificaGPS(int status)
        {
            var settingsBuilder = new LocationSettingsRequest.Builder()
                .AddLocationRequest(mLocationRequest);
            settingsBuilder.SetAlwaysShow(true);
            taskCompletionListener = new TaskCompletionListener();
            taskCompletionListener.Complete += TaskCompletionListenerOnComplete;

            var locationManager = (LocationManager)GetSystemService(Context.LocationService);
            CriaChamadodeLocalizacao();
            if (!locationManager.IsProviderEnabled(LocationManager.GpsProvider))
            {
                taskCompletionListener.Status = status;
                var result = LocationServices.GetSettingsClient(this).CheckLocationSettings(settingsBuilder.Build());
                result.AddOnCompleteListener(this, taskCompletionListener);
                return null;
            }
            else
            {
                switch (status)
                {
                    case 1:
                    case 2:
                        return await PegaMinhaLocalizacaoAsync(20);
                    case 3:
                        return await PegaMinhaLocalizacaoAsync(15);
                }
            }
            return null;
        }
        private void TaskCompletionListenerOnComplete(object sender, EventArgs e)
        {
            try
            {
                var task = (Android.Gms.Tasks.Task)sender;
                LocationSettingsResponse response = (LocationSettingsResponse)task.GetResult(Java.Lang.Class.FromType(typeof(ApiException)));
            }
            catch (ApiException ex)
            {
                switch (ex.StatusCode)
                {
                    case LocationSettingsStatusCodes.ResolutionRequired:
                        try
                        {
                            ResolvableApiException resolvableApiException = (ResolvableApiException)ex;
                            resolvableApiException.StartResolutionForResult(this, REQUEST_CHECK_SETTINGS);
                        }
                        catch (IntentSender.SendIntentException)
                        {

                        }
                        break;
                    case LocationSettingsStatusCodes.SettingsChangeUnavailable:
                        break;
                }
            }
        }

        protected override async void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            switch (requestCode)
            {
                case REQUEST_CHECK_SETTINGS:
                    switch (resultCode)
                    {
                        case Result.Ok:
                            switch (taskCompletionListener.Status)
                            {
                                case 1:
                                case 2:
                                    await PegaMinhaLocalizacaoAsync(20);
                                    break;
                                case 3:
                                    await PegaMinhaLocalizacaoAsync(15);
                                    break;
                            } //Tentando descobrir um jeito que não pegue nulo.
                            break;
                        case Result.Canceled:
                            UserDialogs.Instance.Toast("Você não quis ligar o GPS.", TimeSpan.FromSeconds(2));
                            estaEmUsoLocalizaGPS = false;
                            break;
                    }
                    break;

            }
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (!CheckInternet())
            {
                UserDialogs.Instance.Toast("Você não está conectado!",TimeSpan.FromSeconds(2));
                return false;
            }
            AppDataHelper.GetCurrentUser();
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer((int)GravityFlags.Left);
                    accountTitle = FindViewById<TextView>(Resource.Id.accountTitle);
                    accountCpf = FindViewById<TextView>(Resource.Id.accountCPF);
                    accountEmail = FindViewById<TextView>(Resource.Id.accountEmail);
                    accountPhone = FindViewById<TextView>(Resource.Id.accountPhone);
                    
                    if(user == null)
                    {
                        UserDialogs.Instance.Toast("Não foi possivel carregar informações.");
                    }
                    if (accountTitle != null && user != null)
                    {
                        accountTitle.Text = user.Nome;
                        accountEmail.Text = user.Email;
                        accountCpf.Text = user.Cpf;
                        accountPhone.Text = user.Telefone;
                    }
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }

        }
        public bool CheckInternet()
        {
            var current = Xamarin.Essentials.Connectivity.NetworkAccess;
            if (current == Xamarin.Essentials.NetworkAccess.Internet)
                return true;

            return false;

        }
        public async void OnMapReady(GoogleMap googleMap)
        {
            try
            {
                if (DateTime.Now.Hour >= 0 && DateTime.Now.Hour <= 8)
                {
                    bool sucess = googleMap.SetMapStyle(MapStyleOptions.LoadRawResourceStyle(this, Resource.Raw.night));
                }

            }
            catch
            {

            }
            mainMap = googleMap;
            //mainMap.CameraIdle += MainMap_CameraIdle;
            //string mapkey = Resources.GetString(Resource.String.mapKey);
            //mapHelper = new MapFunctionHelper(mapkey);

            AppDataHelper.GetCurrentUser();
            await VerificaGPS(3);
        }

        private async void MainMap_CameraIdle(object sender, EventArgs e)
        {
            try
            {
                latLng = mainMap.CameraPosition.Target;

                var placemarks = await Xamarin.Essentials.Geocoding.GetPlacemarksAsync(latLng.Latitude, latLng.Longitude);
                var endereco = placemarks?.FirstOrDefault();
                if(endereco != null)
                {
                    Toast.MakeText(this, $"Endereço: {endereco.Thoroughfare} - Número: {endereco.SubThoroughfare} - Bairro: {endereco.SubLocality}", ToastLength.Long).Show();
                }
            }
            catch(Exception ex)
            {
                Toast.MakeText(this,$"error: {ex.Message}", ToastLength.Long).Show();
            }
            //var text = await mapHelper.FindCordinateAddress(latLng);
            
        }

        bool CheckLocationPermission()
        {
            var permissionGranter = false;
            if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.AccessFineLocation) !=
                Android.Content.PM.Permission.Granted &&
                ActivityCompat.CheckSelfPermission(this, Manifest.Permission.AccessCoarseLocation) !=
                Android.Content.PM.Permission.Granted)
            {
                permissionGranter = false;
                RequestPermissions(permissonGroupLocation, requestLocationId);
            }
            else
            {
                permissionGranter = true;
            }

            return permissionGranter;
        }

        void CriaChamadodeLocalizacao()
        {
            mLocationRequest = new LocationRequest();
            mLocationRequest.SetInterval(UPDATE_INTERVAl * 10000);
            mLocationRequest.SetFastestInterval(FASTEST_INTERVAL * 10000);
            mLocationRequest.SetPriority(LocationRequest.PriorityHighAccuracy);
            mLocationRequest.SetSmallestDisplacement(DISPLACEMENT);


            locationClient = LocationServices.GetFusedLocationProviderClient(this);
        }

        private async Task<LatLng> PegaMinhaLocalizacaoAsync(int distancia)
        {
            if (!CheckLocationPermission())
                return null;
            mainMap.Clear();
            try
            {
                CriaChamadodeLocalizacao();
                LatLng minhaPosicao = null;
                System.Collections.Generic.IEnumerable<Xamarin.Essentials.Placemark> placemarks = null;
                Xamarin.Essentials.Placemark endereco = null;
                mLastLocation = await locationClient.GetLastLocationAsync();
                if (mLastLocation != null)
                {
                    minhaPosicao = new LatLng(mLastLocation.Latitude, mLastLocation.Longitude);
                    mainMap.MoveCamera(CameraUpdateFactory.NewLatLngZoom(minhaPosicao, distancia));
                    if (distancia != 15)
                    {
                        placemarks = await Xamarin.Essentials.Geocoding.GetPlacemarksAsync(minhaPosicao.Latitude, minhaPosicao.Longitude);
                        if (placemarks != null)
                        {
                            endereco = placemarks?.FirstOrDefault();
                        }
                        else
                        {
                            throw new Exception("Não foi possivel pegar o endereço!");
                        }
                        if (endereco != null)
                        {
                            endTextView.Text = $"{endereco.Thoroughfare}, {endereco.SubThoroughfare}, {endereco.SubLocality}";
                            mainMap.AddMarker(new MarkerOptions().SetPosition(minhaPosicao).SetTitle("Minha Posição").SetSnippet(endTextView.Text).Draggable(false));
                            
                        }
                        else{
                            throw new Exception("Não foi possível localizar o endereço.");
                        }

                    }
                }
                //Se não achou a posição com o google vai tentar com Xamarin
                else if (mLastLocation == null)
                {
                    var request = new Xamarin.Essentials.GeolocationRequest(Xamarin.Essentials.GeolocationAccuracy.Best);
                    var location = await Xamarin.Essentials.Geolocation.GetLocationAsync(request);
                    if (location != null)
                    {
                        minhaPosicao = new LatLng(location.Latitude, location.Longitude);
                        mainMap.MoveCamera(CameraUpdateFactory.NewLatLngZoom(minhaPosicao, distancia));
                        if (distancia != 15)
                        {
                            placemarks = await Xamarin.Essentials.Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);
                            //var endereco = placemarks?.FirstOrDefault();
                            if (placemarks != null)
                            {
                                endereco = placemarks?.FirstOrDefault();
                            }
                            else
                            {
                                throw new Exception("Não foi possivel pegar o endereço!");
                            }
                            if (endereco != null)
                            {
                                endTextView.Text = $"{endereco.Thoroughfare}, {endereco.SubThoroughfare}, {endereco.SubLocality}";
                                mainMap.AddMarker(new MarkerOptions().SetPosition(minhaPosicao).SetTitle("Minha Posição").SetSnippet(endTextView.Text).Draggable(false));
                            }
                            else
                            {
                                throw new Exception("Não foi possível localizar o endereço.");
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Consulta da sua posição atual falhou!");
                    }
                }
                estaEmUsoLocalizaGPS = false;
                return minhaPosicao;
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("Location services are not enabled on device."))
                {
                    // Unable to get location
                    UserDialogs.Instance.Toast("GPS não está ligado", TimeSpan.FromSeconds(5));
                }else if(ex.Message.Contains("grpc failed"))
                {
                    UserDialogs.Instance.Toast("Não foi possível localizar o endereço.", TimeSpan.FromSeconds(5));
                }
                else
                {
                    UserDialogs.Instance.Toast(ex.Message, TimeSpan.FromSeconds(5));
                }
                estaEmUsoLocalizaGPS = false;
                return null;
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [Android.Runtime.GeneratedEnum] Permission[] grantResults)
        {
            if (grantResults.Length < 1)
            {
                Snackbar.Make(rootview, "Permissão Garantida.", 5);
                return;
            }
            if (grantResults[0] == (int)Android.Content.PM.Permission.Granted)
            {
                
            }
            else
            {
                Snackbar.Make(rootview, "Permissão negada.", 5);
            }
        }
    }
}
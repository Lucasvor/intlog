﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Firebase.Auth;
using IntLog.Helpers;

namespace IntLog.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Thread.Sleep(1000);
            // Create your application here
        }

        protected override void OnResume()
        {
            base.OnResume();
            //StartActivity(typeof(MainActivity));
            FirebaseUser currentUser = AppDataHelper.GetCurrentUser();
            if (currentUser == null || AppDataHelper.IsLogin())
            {
                StartActivity(typeof(LoginActivity));
            }
            else
            {
                StartActivity(typeof(MainActivity));
            }
            //StartActivity(typeof(MainActivity));
        }
    }
}
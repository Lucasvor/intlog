# Aplicativo para Registro de entregas - Int Log
Aplicativo com Login para registro de entregas feitas pelo motorista de acordo com sua posição no mapa (Google Maps), com a possiblidade de alteração do seu ponto atual com o click no mapa. Ao fazer entrega é necessário fazer a marcação no sistema de entrega, onde no aplicativo é possível escolher o motivo da entrega e com isso ler o código de barras da carta, em que essa informações é enviada para o banco e processada, podendo ser vista na aba de Histórico ou excluida. 

# Tecnologias usadas
* **C#** (Linguagem principal para codificação de todo código)
* **Xamarin** (Framework de desenvolvimento mobile utilizando C#)
* **Xamarin Essentials** (Biblioteca utilizada para facilitar o uso de métodos fornecidos pelo celular para C#)
* **Google Maps** (Para ajudar o usuário localizar onde está ou indicar área correta proximo a ele)
* **Biblioteca Location** (Biblioteca de métodos para pegar a localização do usuário)
* **Firebase** (Serviço administrado pelo google para Autenticação e armazenamento dos registro)
* **Acr.UserDialogs** (Para facilitar o uso de Action Sheet, Alert, Confirm, Date, Loading, Login, Toats, Time em C#)
* **Newtonsoft.Json** (Biblioteca para facilitar o uso de JSON)
* **ZXing.Net.Mobile** (Biblioteca para uso de camera e reconhecimento de código de barras e QRCode)

# Imagens do Aplicativo
* Splash Screen

![Splash Screen](/images/6.png/)
* Login

![Login](/images/7.png/)

* Home

![Home](/images/1.png/)

* Menu Home

![Menu Home](/images/2.png/)

* Tela Motivo

![Tela Motivo](/images/3.png/)

* Leito de QrCode

![Leito de QrCode](/images/4.png/)

* Historico

![Historico](/images/5.png/)
